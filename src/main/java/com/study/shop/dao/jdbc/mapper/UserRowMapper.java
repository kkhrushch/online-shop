package com.study.shop.dao.jdbc.mapper;

import com.study.shop.entity.User;
import com.study.shop.security.entity.UserRole;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRowMapper {
    public User mapRow(ResultSet resultSet) throws SQLException {
        User user = new User();

        String username = resultSet.getString(1);
        String password = resultSet.getString(2);
        String userRole = resultSet.getString(3);

        user.setName(username);
        user.setPassword(password);
        user.setUserRole(UserRole.valueOf(userRole));

        return user;
    }
}
