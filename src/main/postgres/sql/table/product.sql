CREATE TABLE public.product
(
    id bigserial NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    price double precision,
    add_date date NOT NULL,
    picture_path text COLLATE pg_catalog."default" NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.product
    OWNER to postgres;